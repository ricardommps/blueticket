## Clone project
```bash
git clone https://ricardommps@bitbucket.org/ricardommps/blueticket.git
```

##  How to install
```bash
cd blueticket
npm install
```
### Start the client
```bash
npm run dev
```