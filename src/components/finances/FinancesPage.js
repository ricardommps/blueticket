// @flow

import React, { Component } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux';
import { FETCH_FINANCES_IF_NEEDED, FETCH_FINANCES  } from '../../actionTypes'
import { selectFinances} from '../../selectors/finances'
import navigateTo from '../../services/navigation'

import type { Dispatch, State } from '../../types'
import type { FinancesState } from '../../types/finances'
import type { Connector } from 'react-redux'

import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Chip from '@material-ui/core/Chip';
import IconButton from '@material-ui/core/IconButton';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';

import {  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend } from 'recharts';

import moment from 'moment'
import { cloneDeep} from 'lodash';

import {styles} from './styles'


type Props = {
    finances: FinancesState,
    match: {
      url: string
    },
    fetchFinancesIfNeeded(): void,
    fetchFinances(): void
}


  class FinancesPage extends Component<Props> {

    constructor(props: Props) {
      super(props)
      this.state = {
        type:null,
        key:null,
        showChart: false,
        chartItem : []
      }
    }
    componentDidMount() {
      this._loadAllFinances()
    }

    _loadAllFinances = () => {
      this.props.fetchFinances()
    }

    _checkVariation = (n,classes) => {
      let variation = parseFloat(n)
        if(variation < 0){
          return classes.variationNegative
        }
        return classes.variationPositive
        
    }


    handleSelected = (type, key) => {
      const items = cloneDeep(this.props.finances.items.results);
      let update = new Date();
      let time = update.getHours() + ":" + update.getMinutes() + ":" + update.getSeconds();
      items[type][key]['update'] = time
      let chartItem = []
      chartItem.push(items[type][key])
      this.setState({
        chartItem:chartItem,
      })
      this.setState({
        type: type,
        key:key,
        showChart: true
      })
     // this.startTimer()
    }

    startTimer = () => {
      this.timer = setInterval(()=>  this.props.fetchFinances(), 10000);
    }

    stopTimer =() => {
      clearInterval(this.timer)
      this.timer = null;
      this.setState({
        type: null,
        key:null,
        chartItem:[],
        showChart: false
      })
    }

    render() {
      const { classes } = this.props
      const { items: items, loading } = this.props.finances
      const { url } = this.props.match
      const { chartItem } = this.state
      let  displayChart = this.state.showChart ? 'grid' : 'none'
      return (
        <div className="container">
          {
            items.results && 
              <div>
                  <CssBaseline />
                  <Container maxWidth="xl" className={classes.container}>
                      <div className={classes.title}>
                        <Typography variant="h4" gutterBottom>
                          COTAÇÃO DAS PRINCIPAIS MOEDAS PARA O REAL
                        </Typography>
                      </div>
                      <Grid container spacing={3}>
                        <Grid item xs={6} sm={3}>
                            <div className={classes.results} 
                                onClick={() => this.handleSelected('currencies','USD')}
                            >
                              <Chip label={items.results.currencies.USD.variation} 
                                className={`${classes.variation} ${this._checkVariation(items.results.currencies.USD.variation,classes)}`}/>
                              <Typography variant="h4" gutterBottom className={classes.price}>
                                  R$ {items.results.currencies.USD.buy}
                              </Typography>
                              <Typography variant="subtitle1" gutterBottom className={classes.name}>
                                {items.results.currencies.USD.name}
                              </Typography>
                            </div>
                            
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <div className={classes.results}
                              onClick={() => this.handleSelected('currencies','EUR')}
                            >
                              <Chip label={items.results.currencies.EUR.variation} 
                                className={`${classes.variation} ${this._checkVariation(items.results.currencies.EUR.variation,classes)}`}/>
                              <Typography variant="h4" gutterBottom className={classes.price}>
                                  R$ {items.results.currencies.EUR.buy}
                              </Typography>
                              <Typography variant="subtitle1" gutterBottom className={classes.name}>
                                {items.results.currencies.EUR.name}
                              </Typography>
                            </div>
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <div className={classes.results}
                                onClick={() => this.handleSelected('currencies','ARS')}
                            >
                              <Chip label={items.results.currencies.ARS.variation} 
                                className={`${classes.variation} ${this._checkVariation(items.results.currencies.ARS.variation,classes)}`}/>
                              <Typography variant="h4" gutterBottom className={classes.price}>
                                  R$ {items.results.currencies.ARS.buy}
                              </Typography>
                              <Typography variant="subtitle1" gutterBottom className={classes.name}>
                                {items.results.currencies.ARS.name}
                              </Typography>
                            </div>
                        </Grid>
                        <Grid item xs={6} sm={3}>
                            <div className={classes.results}
                                onClick={() => this.handleSelected('currencies','BTC')}
                            >
                              <Chip label={items.results.currencies.BTC.variation} 
                                className={`${classes.variation} ${this._checkVariation(items.results.currencies.BTC.variation,classes)}`}/>
                              <Typography variant="h4" gutterBottom className={classes.price}>
                                  R$ {items.results.currencies.BTC.buy}
                              </Typography>
                              <Typography variant="subtitle1" gutterBottom className={classes.name}>
                                {items.results.currencies.BTC.name}
                              </Typography>
                            </div>
                        </Grid>
                      </Grid>
                      <Divider className={classes.divider} />
                      <div className={classes.title}>
                        <Typography variant="h4" gutterBottom>
                          ÍNDICES DE BOLSAS DE VALORES PELO MUNDO
                        </Typography>
                      </div>
                      <Grid container spacing={3}>
                        <Grid item xs={6} sm={3}>
                            <div className={classes.results}
                                onClick={() => this.handleSelected('stocks','IBOVESPA')}
                            >
                              <Chip label={items.results.stocks.IBOVESPA.variation} 
                                className={`${classes.variation} ${this._checkVariation(items.results.stocks.IBOVESPA.variation,classes)}`}/>
                              <Typography variant="h4"className={classes.price}>
                                 {items.results.stocks.IBOVESPA.points ? items.results.stocks.IBOVESPA.points : '-'}
                              </Typography>
                              <Typography variant="subtitle1" gutterBottom className={classes.name}>
                                {items.results.stocks.IBOVESPA.name}
                              </Typography>
                            </div>
                            
                        </Grid>
                        <Grid item xs={6} sm={3}>
                          <div className={classes.results}
                              onClick={() => this.handleSelected('stocks','NASDAQ')}
                          >
                              <Chip label={items.results.stocks.NASDAQ.variation} 
                                className={`${classes.variation} ${this._checkVariation(items.results.stocks.NASDAQ.variation,classes)}`}/>
                              <Typography variant="h4"className={classes.price}>
                                 {items.results.stocks.NASDAQ.points ? items.results.stocks.NASDAQ.points : '-'}
                              </Typography>
                              <Typography variant="subtitle1" gutterBottom className={classes.name}>
                                {items.results.stocks.NASDAQ.name}
                              </Typography>
                            </div>
                        </Grid>
                        <Grid item xs={6} sm={3}>
                          <div className={classes.results}
                           onClick={() => this.handleSelected('stocks','CAC')}
                          >
                              <Chip label={items.results.stocks.CAC.variation} 
                                className={`${classes.variation} ${this._checkVariation(items.results.stocks.CAC.variation,classes)}`}/>
                              <Typography variant="h4"className={classes.price}>
                                 {items.results.stocks.CAC.points ? items.results.stocks.CAC.points : '-'}
                              </Typography>
                              <Typography variant="subtitle1" gutterBottom className={classes.name}>
                                {items.results.stocks.CAC.name}
                              </Typography>
                            </div>
                        </Grid>
                        <Grid item xs={6} sm={3}>
                          <div className={classes.results}
                              onClick={() => this.handleSelected('stocks','NIKKEI')}
                          >
                              <Chip label={items.results.stocks.NIKKEI.variation} 
                                className={`${classes.variation} ${this._checkVariation(items.results.stocks.NIKKEI.variation,classes)}`}/>
                              <Typography variant="h4"className={classes.price}>
                                 {items.results.stocks.NIKKEI.points ? items.results.stocks.NIKKEI.points : '-'}
                              </Typography>
                              <Typography variant="subtitle1" gutterBottom className={classes.name}>
                                {items.results.stocks.NIKKEI.name}
                              </Typography>
                            </div>
                        </Grid>
                      </Grid>
                      <Divider className={classes.divider} />
                      <Card className={classes.card}  style={{ display: displayChart }}>
                        <CardHeader
                          action={
                            <IconButton aria-label="settings" onClick={() => this.stopTimer()}>
                              <MoreVertIcon />
                            </IconButton>
                          }
                          title="Shrimp and Chorizo Paella"
                        />
                         <CardContent>
                            <LineChart
                                width={700}
                                height={300}
                                data={chartItem}
                                margin={{top: 5, right: 30, left: 20, bottom: 5}}
                                >
                                <Line
                                type='monotone'
                                dataKey='buy'
                                stroke='#8884d8'
                                activeDot={{r: 8}}
                                />
                                <CartesianGrid strokeDasharray='3 3'/>
                                <Tooltip/>
                                <YAxis/>
                                <XAxis dataKey='update'/>
                                <Legend />
                            </LineChart>
                         </CardContent>
                      </Card>



                  </Container>
              </div>
          }
        </div>
      )
    }
  }
  
  function mapStateToProps(state: State) {
    return {
      finances: selectFinances(state)
    }
  }
  
  function mapDispatchToProps(dispatch: Dispatch) {
    return {
        fetchFinancesIfNeeded: () => dispatch({ type: FETCH_FINANCES_IF_NEEDED }),
        fetchFinances: () => dispatch({ type: FETCH_FINANCES })
    }
  }
  
  const connector: Connector<{}, Props> = connect(
    mapStateToProps,
    mapDispatchToProps
  )

  export default compose(
    withStyles(styles),
    connector,
)(FinancesPage);
 