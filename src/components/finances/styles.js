export const styles = theme => ({
    root: {
        display: 'flex'
    },
    container: {
        backgroundColor: '#313a46'
    },
    results: {
        padding: theme.spacing(2),
        textAlign: 'center',
        cursor: 'pointer'
        
    },
    divider: {
        margin: theme.spacing(2, 0),
        backgroundColor: '#fff'
    },
    title: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: '#fff'
    },
    name: {
        fontSize: '13px',
        color: '#fff'
    },
    price: {
        color: '#fff',
        fontWeight: '400',
    },
    variation : {
        fontSize: '13px',
        height: '20px',
        marginBottom: '8px',
        color: '#fff'
    },

    variationNegative: {
        backgroundColor: '#fa5c7c'
    },
    variationPositive: {
        backgroundColor: '#0acf97'
    },
    card: {
        maxWidth: '100%',
    },
})