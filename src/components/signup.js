import React, { Component, Fragment } from "react";
import { withRouter, Link} from "react-router-dom"
import navigateTo from '../services/navigation'
import Snackbar from '@material-ui/core/Snackbar';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { withStyles } from '@material-ui/core/styles';
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import WarningIcon from '@material-ui/icons/Warning';
import { connect } from 'react-redux'
import {styles} from './styles'


class SignUp extends Component {

    constructor(props) {
        super(props);

        this.state = {
            user: {
                email: '',
                password: '',
            },
            submitted: false,
            vertical: 'top',
            horizontal: 'center',
            openAlert:false
        };
    }

    handleCloseAlert = () => {
        this.setState({
            openAlert:false
        })
    }

    handleChange = (event) => {
        const { name, value } = event.target;
        const { user } = this.state;
        this.setState({
            user: {
                ...user,
                [name]: value
            }
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        this.setState({ submitted: true });
        const { user} = this.state;
        this.setState({ submitted: true });
        if (user.email && user.password) {
            const persistedUsers = localStorage.getItem('users') ? JSON.parse(localStorage.getItem('users')) : null
            if(persistedUsers){
                try{
                    let findUser = persistedUsers.users.filter( obj => obj.email === user.email)[0];
                    if(!findUser){
                        persistedUsers.users.push(user)
                        localStorage.setItem('users', JSON.stringify(persistedUsers))
                        navigateTo('/')
                    }else{
                        this.setState({
                            openAlert:true
                        })
                    }
                }
                catch (e) {
                    this.setState({
                        openAlert:true
                    })
                }
                
            }else{
                let saveUsers = {
                    users: []
                }
                saveUsers.users.push(user)
                localStorage.setItem('users', JSON.stringify(saveUsers))
                navigateTo('/')
            }
        }
    }

   
    render() {
        const { classes } = this.props
        const { user, submitted, openAlert, vertical, horizontal } = this.state;
        return (
            <Fragment>
                <div className="d-flex justify-content-center p-5">
                    <div className="col-md-4 col-md-offset-3">
                        <h2>Register</h2>
                        <form name="form">
                            <div className={'form-group' + (submitted && !user.email ? ' has-error' : '')}>
                                <label htmlFor="email">E-mail</label>
                                <input type="text" 
                                    className="form-control" 
                                    name="email" value={user.email} 
                                    onChange={this.handleChange} />
                                {submitted && !user.email &&
                                    <div className="help-block">E-mail is required</div>
                                }
                            </div>
                            <div className={'form-group' + (submitted && !user.password ? ' has-error' : '')}>
                                <label htmlFor="password">Password</label>
                                <input type="password" className="form-control" name="password" value={user.password} onChange={this.handleChange} />
                                {submitted && !user.password &&
                                    <div className="help-block">Password is required</div>
                                }
                            </div>  
                            <div className="form-group">
                                <button 
                                    className="btn btn-primary btn-lg" 
                                    onClick={(event) => this.handleSubmit(event)}
                                >Register</button>
                                <Link to="/" className="btn btn-link btn-lg">Cancel</Link>
                            </div>  
                        </form>
                        <Snackbar
                            anchorOrigin={{ vertical, horizontal }}
                            open={openAlert}
                            onClose={this.handleCloseAlert}
                            autoHideDuration={6000}
                        > 
                            <SnackbarContent
                                className={classes.warning}
                                aria-describedby="snackbar"
                                message={
                                    <span id="snackbar" className={classes.message}>
                                     <WarningIcon className={classes.iconVariant} />
                                        E-mail ja cadastrado
                                    </span>
                                }
                            />
                        </Snackbar>
                    </div>
                </div>
            </Fragment>
        )
    }
}
export default withRouter(connect()(withStyles(styles)(SignUp)))
