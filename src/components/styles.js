import { amber } from '@material-ui/core/colors';
export const styles = theme => ({
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing(1),
      },
      message: {
        display: 'flex',
        alignItems: 'center',
        fontSize: '14px'
      },
      warning: {
        backgroundColor: amber[700],
      }
})