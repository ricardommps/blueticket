// @flow

import type { FinancesState as State, FinancesAction as Action } from '../types/finances'

function finances(
    state: State = {
        items: [],
        loading: false
    },
    action: Action
): State {
    switch (action.type) {
        case 'FETCH_FINANCES_PENDING':
            return {
                ...state,
                loading: true
            }
        case 'FETCH_FINANCES_SUCCESS':
            return {
                items: action.payload,
                loading: false
            }
        case 'FETCH_FINANCES_FAILURE':
            return {
                items: [],
                loading: false
            }
        default:
            return state
    }
}

export default finances