// @flow

import { combineReducers } from 'redux'
import finances from './finances'

const entitiesReducer = combineReducers({
    finances
})

const rootReducer = combineReducers({
    entities: entitiesReducer
})

export default rootReducer