// @flow

import React from 'react'
import { Router, Route, Switch, Redirect } from 'react-router-dom'
import history from './services/history'
import { isAuthenticated } from "./services/auth";
import ErrorPage from './components/ErrorPage'
import SignIn from "./components/signIn";
import Signup from "./components/signup";
import FinancesPage from './components/finances/FinancesPage'

const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
      {...rest}
      render={props =>
        isAuthenticated() ? (
          <Component {...props} />
        ) : (
          <Redirect to={{ pathname: "/", state: { from: props.location } }} />
        )
      }
    />
  );

const Routes = () => (
    <Router history={history}>
      <div>
        <Switch>
            <Route exact path="/" component={SignIn} />
            <Route exact path="/signup" component={Signup} />
            <PrivateRoute path="/admin" component={FinancesPage} />
            <Route path="/error" component={ErrorPage} />
        </Switch>
      </div>
    </Router>
)

export default Routes;