import { takeLatest, put, call, select } from 'redux-saga/effects'
import { fetchFinancesFromApi } from '../../services/finances'
import navigateTo from '../../services/navigation'
import { selectFinances } from '../../selectors/finances'

function* fetchFinances() {
    yield put({
        type: 'FETCH_FINANCES_PENDING'
    })

    try {
        const financesFromApi = yield call(fetchFinancesFromApi)
        yield put({
            type: 'FETCH_FINANCES_SUCCESS',
            payload: financesFromApi
        })
    } catch (error) {
        yield put({
            type: 'FETCH_FINANCES_FAILURE'
        })
        yield put(navigateTo('/error'))
    }
}

export function* watchFetchFinances() {
    yield takeLatest('FETCH_FINANCES', fetchFinances)
}

function* fetchFinancesIfNeeded() {
    const { items: finances } = yield select(selectFinances)
    if (finances.length === 0) {
        yield call(fetchFinances)
    }
}

export function* watchFetchFinancesIfNeeded() {
    yield takeLatest('FETCH_FINANCES_IF_NEEDED', fetchFinancesIfNeeded)
}