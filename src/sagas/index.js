import { all } from 'redux-saga/effects'

import { watchFetchFinancesIfNeeded, watchFetchFinances } from './finances/fetch'

export default function* rootSaga() {
    yield all([
        watchFetchFinancesIfNeeded(),
        watchFetchFinances()
    ])
}