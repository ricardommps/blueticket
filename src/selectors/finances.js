// @flow
import type { State } from '../types'
import type { FinancesState, Finances, Finance } from '../types/finances'

export function selectFinances(state: State): FinancesState {
    return state.entities.finances
}

export function selectCurrentFinances(state: State, id: number): Finances | void {
    const items: Finances = state.entities.finances.items
    return items.find(item => item.id === id)
}