
import axios from 'axios'

import type { Axios } from 'axios'

const API_ROOT = 'https://api.hgbrasil.com/finance?format=json-cors&key=4de8f3fd'

class ApiFinancesService {
    finances: Axios
    constructor() {
      const finances = axios.create({
        baseURL: API_ROOT,
        timeout: 10000,
        headers: {
          'Content-Type': 'application/json'
        }
      })
      this.finances = finances
    }
  
    getFinances(path: string): Promise < Object > | Promise < Array < Object >> {
      return this.finances.get(path).then(response => response.data)
    }
  }
  
  export default new ApiFinancesService()