export const isAuthenticated = () => {
    const persistedUsers = localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : null
    if(persistedUsers){
        const dateString = persistedUsers.timestamp
        const now = new Date();
        if(now.getTime() <= dateString)
        return true
    }else{
        return false
    }
    
};