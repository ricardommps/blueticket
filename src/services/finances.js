import type { Finance, Finances } from '../types/finances'
import service from './ApiFinances'

export function fetchFinancesFromApi(): Promise < Finance > | Promise < Finances > {
    return service.getFinances('')
}
