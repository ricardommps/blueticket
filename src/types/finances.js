// @flow

export type Finances = {
    +name: string,
    +buy: number,
    +sell: number,
    +variation: number
}

export type FinancePayload = $Diff <Finance,{id: number}>
export type Finances = Array <Case>
export type FinancesState = {
        +items: Finances,
        +loading: boolean
    }

export type FinancesAction = 
    | { type: 'FETCH_FINANCES'} 
    | { type: 'FETCH_FINANCES_IF_NEEDED' } 
    | { type: 'FETCH_FINANCES_PENDING' } 
    | { type: 'FETCH_FINANCES_SUCCESS', payload: Finances } 
    | { type: 'FETCH_FINANCES_FAILURE' } 