// @flow

import type { Store as ReduxStore, Dispatch as ReduxDispatch } from 'redux'
import type { FinancesState, FinancesAction } from './finances'

export type ReduxInitAction = {type: '@@INIT'}
export type Action = ReduxInitAction | FinancesAction

export type State = { entities: { 
        finances: FinancesState
    } }

export type Store = ReduxStore < State, Action >
export type Dispatch = ReduxDispatch < Action >